use crate::actions::{CreateFile, Make, Mount, RepoSync, RunCommand, TestFiles};
use crate::command::Command;
use crate::errors::Error;
use crate::validate::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt;
use std::path::PathBuf;

#[derive(Deserialize, Serialize)]
pub enum Action {
    RepoSync(RepoSync),
    Make(Make),
    RunCommand(RunCommand),
    TestFiles(TestFiles),
    CreateFile(CreateFile),
    Mount(Mount),
}

impl fmt::Display for Action {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Action::*;
        match &self {
            RepoSync(_) => write!(f, "RepoSync"),
            Make(_) => write!(f, "Make"),
            RunCommand(_) => write!(f, "RunCommand"),
            TestFiles(_) => write!(f, "TestFiles"),
            CreateFile(_) => write!(f, "WriteFile"),
            Mount(_) => write!(f, "Mount"),
        }
    }
}

#[derive(Deserialize, Serialize, Default)]
pub struct Recipe {
    chrootdir: PathBuf,
    user: Option<String>,
    actions: Vec<HashMap<String, Action>>,
}

impl Recipe {
    pub fn run(&self, active: &[String], simulate: bool) -> Result<(), Error> {
        let mut found = 0;
        for action in &self.actions {
            for (title, action_type) in action {
                // Don't run actions if no match
                // Note if no action is specified ALL action is run
                if !active.is_empty() && active.iter().find(|a| *a == title).is_none() {
                    continue;
                }

                found += 1;
                // default to Recipe.user
                match &*action_type {
                    Action::RepoSync(r) => self.run_type(&title, r, simulate)?,
                    Action::Make(m) => self.run_type(&title, m, simulate)?,
                    Action::TestFiles(t) => self.run_type(&title, t, simulate)?,
                    Action::RunCommand(r) => self.run_type(&title, r, simulate)?,
                    Action::CreateFile(c) => self.run_type(&title, c, simulate)?,
                    Action::Mount(m) => self.run_type(&title, m, simulate)?,
                }
            }
        }

        if active.len() > found {
            return Err(Error::NoValidRunActionsSpecified);
        }

        Ok(())
    }

    pub fn run_type<T>(&self, name: &str, t: &T, simulate: bool) -> Result<(), Error>
    where
        T: Command,
    {
        if simulate {
            println!("'{}' runs: '{}'", name, t.command());
            Ok(())
        } else {
            t.run()
        }
    }

    pub fn list(&self) {
        println!("List action tags:\n");
        for action in &self.actions {
            for (title, _action_type) in action.iter() {
                println!("{}", title);
            }
        }
    }
}

impl fmt::Display for Recipe {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut s = format!("Recipe chroot path: {:?}\n", self.chrootdir);
        for action in &self.actions {
            for (title, action_type) in action {
                s += &format!("{} '{}'\n", title, action_type);
            }
        }
        write!(f, "{}", s)
    }
}

impl Validate for Recipe {
    fn validate(&mut self, _workdir: &PathBuf) -> Result<(), Error> {
        // chroot path must exist
        validate_path(&"".into(), &Some(self.chrootdir.clone()), "chroot.path")?;
        for action in &mut self.actions {
            let chrootdir = self.chrootdir.clone();
            for (_, action_type) in action.iter_mut() {
                match action_type {
                    Action::Make(m) => m.validate(&chrootdir),
                    Action::RunCommand(r) => r.validate(&chrootdir),
                    Action::RepoSync(r) => r.validate(&chrootdir),
                    Action::TestFiles(_) => Ok(()),
                    Action::CreateFile(_) => Ok(()),
                    Action::Mount(_) => Ok(()),
                }?;
            }
        }
        Ok(())
    }
}

impl Recipe {
    pub fn build(&mut self, _: &PathBuf, _: &Option<String>) -> Result<(), Error> {
        for action in &mut self.actions {
            for (_, action_type) in action.iter_mut() {
                match action_type {
                    Action::RepoSync(r) => r.build(&self.chrootdir, &self.user)?,
                    Action::Make(m) => m.build(&self.chrootdir, &self.user)?,
                    Action::RunCommand(r) => r.build(&self.chrootdir, &self.user)?,
                    Action::TestFiles(_) => {}
                    Action::CreateFile(c) => c.build(&self.chrootdir, &self.user)?,
                    Action::Mount(_) => {}
                }
            }
        }
        Ok(())
    }
}

mod tests {
    #[test]
    fn test_se_recipe() {
        use crate::recipe::*;
        let mut r = Recipe {
            chrootdir: "/".into(),
            actions: Vec::new(),
            ..Recipe::default()
        };
        let mut h = std::collections::HashMap::new();
        h.insert("command".into(), Action::RunCommand(RunCommand::default()));
        h.insert("reposync".into(), Action::RepoSync(RepoSync::default()));
        h.insert("make".into(), Action::Make(Make::default()));
        r.actions.push(h);
        println!(
            "{}",
            serde_yaml::to_string(&r).expect("Serialize should work")
        );
    }

    #[test]
    fn test_de_recipe() {
        use crate::recipe::*;
        let yaml = r#"
chrootdir: /tmp
user: developer
actions:
  - apa:
      RunCommand:
        title:
        workdir:
        command: foo kakav
"#;
        let r: Result<Recipe, _> = serde_yaml::from_str(&yaml);
        assert_eq!(true, r.is_ok());

        let yaml = r#"
chrootdir: /tmp
user: developer
actions:
  - RunCommand:
      RunCommand:
        title:
        workdir: /
        command: foo kaka
"#;
        let r: Result<Recipe, _> = serde_yaml::from_str(&yaml);
        assert_eq!(true, r.is_ok());
        let mut r = r.unwrap();
        assert_eq!(true, r.validate(&"".into()).is_ok());

        let yaml = r#"
chrootdir: /tmpasdghjhjklfahjklasdfhjkl
user: developer
actions:
  - tiitle:
      RunCommand:
        title:
        workdir: ./
        command: foo kaka
"#;
        let r: Result<Recipe, _> = serde_yaml::from_str(&yaml);
        let mut r = r.expect("yaml parse ok");
        assert_eq!(true, r.validate(&"".into()).is_err());
        let parent = r.chrootdir;
        assert_eq!(PathBuf::from("/tmpasdghjhjklfahjklasdfhjkl"), parent);
        /* FIXME
        let a = r.actions.get_mut(0).map(|a| {
            if let Some(a) = a.get_mut("myaction") {
                Ok(a)
            } else {
                Err(Error::MissingField(""))
            }
        }).expect("RunAction").unwrap_or_else(|_|panic!("No run action!"));

        assert_eq!(true, a.chroot);
        assert_eq!(true, a.validate(&"/".into()).is_ok());
        */
        let yaml = r#"
chrootdir: /tmp
user: developer
actions:
  - title:
      RunCommand:
        title:
        workdir: tmp
        command: foo kaka
"#;
        let r: Result<Recipe, _> = serde_yaml::from_str(&yaml);
        let mut r = r.expect("yaml parse ok");
        assert_eq!(true, r.validate(&"/".into()).is_ok());
        let parent = r.chrootdir;
        assert_eq!(PathBuf::from("/tmp"), parent);
        /* FIXME
        let a = r.actions.get_mut(0).map(|a| {
            for (_, at) in a {
                match at {
                    Action::RunCommand(a) => Ok(a),
                    _ => Err(Error::MissingField(""))
                };
            }
        }).expect("RunAction").unwrap_or_else(|_|panic!("No run action!"));

        assert_eq!(true, a.chroot);
        assert_eq!(true, a.validate(&parent).is_ok());
        */
    }
}
