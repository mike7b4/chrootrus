use crate::errors::Error;
use std::fmt;
use std::process::{Command, Stdio};
#[derive(Debug)]
pub struct Sh {
    cmd: String,
    args: Vec<String>,
}

impl Sh {
    pub fn command(user: &Option<String>, command: &str) -> Self {
        let mut args: Vec<String> = Vec::new();
        // Note overwritten if user is set since we need sudo
        let mut cmd = String::from("/bin/sh");
        if let Some(ref user) = user {
            cmd = "sudo".into();
            args.push("su".into());
            args.push("-l".into());
            args.push(user.clone());
            args.push("/bin/sh".into());
        }
        args.push("-c".into());
        // jikes that pathbuf...
        args.push(command.to_string());
        Self { cmd, args }
    }

    pub fn run(&self) -> Result<(), Error> {
        println!("{:?}", &self.args);
        // TODO there is no point run sudo if user are same...
        let mut child = Command::new(&self.cmd)
            .args(&self.args)
            .stdin(Stdio::piped())
            .spawn()
            .unwrap_or_else(|_| panic!("'{:?}' failed", self.args));

        let exitstatus = child.wait();
        if let Ok(exitstatus) = exitstatus {
            if !exitstatus.success() {
                return Err(Error::CommandFailed(
                    self.args.join(" "),
                    exitstatus.code().unwrap_or(127),
                ));
            }
            Ok(())
        } else {
            panic!("WTF should not reach this");
        }
    }
}

impl fmt::Display for Sh {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {}", self.cmd, self.args.join(" "))
    }
}

mod tests {
    #[test]
    fn test_sh() {
        use crate::execute::Sh;
        let cmd = Sh::command(&None, "echo hello iii");
        assert_eq!(
            String::from(r#"/bin/sh -c echo hello iii"#),
            cmd.to_string()
        );

        let cmd = Sh::command(&Some("login".into()), "echo hello ooo");
        assert_eq!(
            String::from(r#"sudo su -l login /bin/sh -c echo hello ooo"#),
            cmd.to_string()
        );
    }
}
