use crate::errors::Error;
use std::path::PathBuf;

pub trait Command {
    fn build(&mut self, chrootdir: &PathBuf, user: &Option<String>) -> Result<(), Error>;
    fn command(&self) -> String;
    fn run(&self) -> Result<(), Error>;
}

#[derive(Debug)]
pub struct CommandBuilder {
    // chrootdir + work
    chworkdir: PathBuf,
    args: Vec<String>,
}

impl CommandBuilder {
    pub fn new(chrootdir: &PathBuf) -> Result<Self, Error> {
        if chrootdir != &PathBuf::new() {
            std::fs::metadata(chrootdir)
                .map(|d| d.is_dir())
                .map_err(|_| Error::PathNotExist("chrootdir", chrootdir.clone()))?;
        }

        Ok(Self {
            chworkdir: chrootdir.clone(),
            args: Vec::new(),
        })
    }

    pub fn cd(&mut self, workdir: &PathBuf, check_exist: bool) -> Result<&mut Self, Error> {
        let chworkdir: PathBuf = PathBuf::from(format!(
            "{}/{}",
            self.chworkdir.to_string_lossy(),
            workdir.to_string_lossy()
        ));
        if check_exist {
            std::fs::metadata(&chworkdir)
                .map(|d| d.is_dir())
                .map_err(|_| Error::PathNotExist("workdir", workdir.clone()))?;
        }
        self.chworkdir = chworkdir;
        self.args.push(format!("cd {}", workdir.to_string_lossy()));
        Ok(self)
    }

    #[allow(dead_code)]
    pub fn mkdir(&mut self, dir: &PathBuf, parent: bool) -> Result<(), Error> {
        if parent {
            self.args
                .push(format!("mkdir -p {}", dir.to_string_lossy()));
        } else {
            self.args.push(format!("mkdir {}", dir.to_string_lossy()));
        }
        Ok(())
    }

    pub fn push(&mut self, arg: &str) -> &mut Self {
        self.args.push(arg.into());
        self
    }

    #[allow(dead_code)]
    pub fn echo(&mut self, info: &str) -> &mut Self {
        self.args.push(format!("echo '{}'", info));
        self
    }

    pub fn make(
        &mut self,
        workdir: &Option<PathBuf>,
        args: &Option<String>,
    ) -> Result<&mut Self, Error> {
        let mut s = "make".to_string();
        if let Some(w) = &workdir {
            // We cant use push here since workdir is absolute.
            let win: &PathBuf = &PathBuf::from(format!(
                "{}/{}",
                self.chworkdir.to_string_lossy(),
                w.to_string_lossy()
            ));
            std::fs::metadata(win)
                .map(|d| d.is_dir())
                .map_err(|_| Error::PathNotExist("make.workdir", win.clone()))?;
            s += &format!(" -C {}", w.to_string_lossy());
        }
        if let Some(a) = args {
            s += &format!(" {}", a);
        }

        self.args.push(s);
        Ok(self)
    }

    pub fn repo_sync(&mut self, branch: &Option<String>, manifest: &Option<String>) -> &mut Self {
        if let Some(branch) = branch {
            if let Some(manifest) = manifest {
                self.push(&format!("repo init -b {} -m {}", branch, manifest));
            } else {
                self.push(&format!("repo init -b {}", branch));
            }
        }
        self.push("repo sync");
        self
    }

    pub fn finish(&self) -> String {
        self.args.join(" && ")
    }
}

mod tests {
    #[test]
    fn test_chroot_exists() {
        use crate::command::*;
        assert_eq!(
            true,
            CommandBuilder::new(&"/epicfail252526437847849".into()).is_err()
        );
        assert_eq!(true, CommandBuilder::new(&"/tmp".into()).is_ok());
    }

    #[test]
    fn test_cd() {
        use crate::command::*;
        let mut c = CommandBuilder::new(&"/".into()).unwrap_or_else(|e| panic!(e));
        // path Must exists
        assert_eq!(true, c.cd(&"/tmp".into(), true).is_ok());
        assert_eq!(1, c.args.len());
        assert_eq!(String::from("cd /tmp"), c.finish());

        // Add a second path without check
        assert_eq!(true, c.cd(&"/tmp2".into(), false).is_ok());
        assert_eq!(String::from("cd /tmp && cd /tmp2"), c.finish());

        // mkdir
        assert_eq!(true, c.mkdir(&"/tmp3".into(), false).is_ok());
        assert_eq!(
            String::from("cd /tmp && cd /tmp2 && mkdir /tmp3"),
            c.finish()
        );

        // mkdir
        assert_eq!(true, c.mkdir(&"/tmp4/foo".into(), false).is_ok());
        assert_eq!(
            String::from("cd /tmp && cd /tmp2 && mkdir /tmp3 && mkdir /tmp4/foo"),
            c.finish()
        );
    }

    #[test]
    fn test_echo() {
        // echo
        use crate::command::*;
        let mut c = CommandBuilder::new(&"/".into()).unwrap_or_else(|e| panic!(e));
        c.echo("echo apa");
        assert_eq!(String::from("echo 'echo apa'"), c.finish());

        // echo
        c.echo("rust");
        assert_eq!(String::from("echo 'echo apa' && echo 'rust'"), c.finish());
    }

    #[test]
    fn test_push() {
        // echo
        use crate::command::*;
        let mut c = CommandBuilder::new(&"/".into()).unwrap_or_else(|e| panic!(e));
        c.push("ls");
        assert_eq!(String::from("ls"), c.finish());

        // echo
        c.push("cat foo");
        assert_eq!(String::from("ls && cat foo"), c.finish());
    }

    #[test]
    fn test_repo() {
        use crate::command::*;
        let mut c = CommandBuilder::new(&"/".into()).unwrap_or_else(|e| panic!(e));
        c.repo_sync(&None);
        assert_eq!(String::from("repo sync"), c.finish());

        let mut c = CommandBuilder::new(&"/".into()).unwrap_or_else(|e| panic!(e));
        c.repo_sync(&Some("master".into()));
        assert_eq!(
            String::from("repo checkout master && repo sync"),
            c.finish()
        );
    }

    #[test]
    fn test_make() {
        // make no workdir specified
        use crate::command::*;
        let mut c = CommandBuilder::new(&"/".into()).unwrap_or_else(|e| panic!(e));
        assert_eq!(true, c.make(&None, &None).is_ok());
        assert_eq!(String::from("make"), c.finish());

        let mut c = CommandBuilder::new(&"/home".into()).unwrap_or_else(|e| panic!(e));
        assert_eq!(
            true,
            c.make(&Some("/epicfail1234567890".into()), &None).is_err()
        );
        assert_eq!(String::from(""), c.finish());

        let mut c = CommandBuilder::new(&"/usr".into()).unwrap_or_else(|e| panic!(e));
        assert_eq!(true, c.make(&Some("/bin".into()), &None).is_ok());
        assert_eq!(String::from("make -C /bin"), c.finish());

        let mut c = CommandBuilder::new(&"/usr".into()).unwrap_or_else(|e| panic!(e));
        assert_eq!(true, c.make(&Some("bin".into()), &None).is_ok());
        assert_eq!(String::from("make -C bin"), c.finish());

        assert_eq!(true, c.cd(&"local".into(), true).is_ok());
        assert_eq!(String::from("make -C bin && cd local"), c.finish());
        assert_eq!(true, c.make(&Some("bin".into()), &None).is_ok());
        assert_eq!(
            String::from("make -C bin && cd local && make -C bin"),
            c.finish()
        );
    }
}
