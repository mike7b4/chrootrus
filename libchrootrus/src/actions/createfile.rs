use crate::command::Command;
use crate::errors::Error;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fs;
use std::io::Write;
use std::os::unix::fs::OpenOptionsExt;
use std::path::PathBuf;

#[derive(Deserialize, Serialize)]
pub struct CreateFile {
    #[serde(default)]
    chroot: bool,
    #[serde(default)]
    overwrite: bool,
    #[serde(default = "def_true")]
    create_dir: bool,
    file_name: PathBuf,
    #[serde(default = "def_mode")]
    mode: u32,
    input: String,
}

fn def_mode() -> u32 {
    0o644
}
fn def_true() -> bool {
    true
}

impl Command for CreateFile {
    fn build(&mut self, _: &PathBuf, _: &Option<String>) -> Result<(), Error> {
        Ok(())
    }

    fn command(&self) -> String {
        format!("{}", self)
    }

    fn run(&self) -> Result<(), Error> {
        if self.create_dir {
            if let Some(path) = &self.file_name.parent() {
                fs::create_dir(path)
                    .unwrap_or_else(|e| eprintln!("Create dir: '{:?}': {}", path, e));
            }
        }

        if fs::metadata(&self.file_name).is_ok() && !self.overwrite {
            return Err(Error::FileExists(self.file_name.clone()));
        }

        let mut file = fs::OpenOptions::new()
            .write(true)
            .mode(self.mode)
            .create(true)
            .open(&self.file_name)
            .map_err(|e| Error::IO(e, self.file_name.clone()))?;
        file.write_all(self.input.as_bytes())?;

        Ok(())
    }
}

impl fmt::Display for CreateFile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Create file: {} ({:o})", self.file_name.to_string_lossy(), self.mode)
    }
}
