use crate::chroot::ChRoot;
use crate::command::{Command, CommandBuilder};
use crate::errors::Error;
use crate::validate::*;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::path::PathBuf;
#[derive(Deserialize, Serialize, Default)]
pub struct Make {
    workdir: Option<PathBuf>,
    args: Option<String>,
    #[serde(skip)]
    output: String,
    #[serde(skip)]
    chroot: Option<ChRoot>,
}

impl Validate for Make {
    fn validate(&mut self, _: &PathBuf) -> Result<(), Error> {
        Ok(())
    }
}

impl Command for Make {
    fn build(&mut self, chrootdir: &PathBuf, user: &Option<String>) -> Result<(), Error> {
        let mut c = CommandBuilder::new(chrootdir)?;
        c.make(&self.workdir, &self.args)?;
        self.output = c.finish();
        self.chroot = Some(ChRoot::command(chrootdir, user, &self.output));
        Ok(())
    }

    fn command(&self) -> String {
        self.output.clone()
    }

    fn run(&self) -> Result<(), Error> {
        if let Some(r) = &self.chroot {
            return r.run();
        }
        panic!("chroot None forget to call build()?")
    }
}

impl fmt::Display for Make {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.command())
    }
}

mod tests {
    #[test]
    fn test_make() {
        use crate::actions::make::*;
        let chrootdir = PathBuf::from("/usr");
        let mut m = Make {
            workdir: Some("bin".into()),
            ..Make::default()
        };
        assert_eq!(true, m.build(&chrootdir, &None).is_ok());
        assert_eq!("make -C bin", m.command());

        let mut m = Make {
            workdir: Some("bin".into()),
            args: Some("-D aka".into()),
            ..Make::default()
        };
        assert_eq!(true, m.build(&chrootdir, &None).is_ok());
        assert_eq!("make -C bin -D aka", m.command());

        let mut m = Make {
            workdir: None,
            args: Some("-D aka".into()),
            ..Make::default()
        };
        assert_eq!(true, m.build(&chrootdir, &None).is_ok());
        assert_eq!("make -D aka", m.command());
        let mut m = Make { ..Make::default() };
        assert_eq!(true, m.build(&chrootdir, &None).is_ok());
        assert_eq!("make", m.command());

        // should fail dir not exist
        let mut m = Make {
            workdir: Some("dir does not exist".into()),
            ..Make::default()
        };
        assert_eq!(true, m.build(&chrootdir, &None).is_err());
    }
}
