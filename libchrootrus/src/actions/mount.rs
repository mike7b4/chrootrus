use crate::command::Command;
use crate::errors::Error;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::path::PathBuf;
use std::process::Stdio;

#[derive(Deserialize, Serialize)]
pub struct Mount {
    #[serde(default = "def_true")]
    sysfs: bool,
    #[serde(default = "def_true")]
    proc: bool,
    #[serde(default = "def_true")]
    smb: bool,
    #[serde(skip)]
    mounts: Vec<Vec<String>>,
}

fn def_true() -> bool {
    true
}

impl Command for Mount {
    fn build(&mut self, chrootdir: &PathBuf, _: &Option<String>) -> Result<(), Error> {
        if self.proc {
            let mut args: Vec<String> = Vec::new();
            args.push("mount".into());
            args.push("-t".into());
            args.push("proc".into());
            args.push("proc".into());
            args.push(format!("{}/proc", chrootdir.to_string_lossy()));
            self.mounts.push(args);
        }
        if self.sysfs {
            let mut args: Vec<String> = Vec::new();
            args.push("mount".into());
            args.push("--rbind".into());
            args.push("/sys".into());
            args.push(format!("{}/sys", chrootdir.to_string_lossy()));
            self.mounts.push(args);
        }
        Ok(())
    }

    fn command(&self) -> String {
        format!("{:?}", &self.mounts)
    }

    fn run(&self) -> Result<(), Error> {
        for command in &self.mounts {
            self.mount(command)?;
        }
        Ok(())
    }
}

impl Mount {
    fn mount(&self, args: &[String]) -> Result<(), Error> {
        let mut child = std::process::Command::new("sudo")
            .args(args)
            .stdin(Stdio::piped())
            .spawn()
            .unwrap_or_else(|_| panic!("'{:?}' failed", args));

        let exitstatus = child.wait();
        if let Ok(exitstatus) = exitstatus {
            if !exitstatus.success() {
                return Err(Error::CommandFailed(
                    args.join(" "),
                    exitstatus.code().unwrap_or(127),
                ));
            }
            Ok(())
        } else {
            panic!("WTF should not reach this");
        }
    }
}

impl fmt::Display for Mount {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(
            f,
            "sysfs: {}\nproc: {}\nsmb: {}",
            self.sysfs, self.proc, self.smb
        )
    }
}
