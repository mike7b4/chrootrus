use crate::chroot::ChRoot;
use crate::command::{Command, CommandBuilder};
use crate::errors::Error;
use crate::execute::Sh;
use crate::validate::*;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::path::PathBuf;
#[derive(Deserialize, Serialize, Default)]
pub struct RunCommand {
    #[serde(default = "def_true")]
    pub chroot: bool,
    pub user: Option<String>,
    workdir: Option<PathBuf>,
    command: Option<String>,
    commands: Option<Vec<String>>,
    #[serde(skip)]
    output: String,
    #[serde(skip)]
    rchroot: Option<ChRoot>,
}

fn def_true() -> bool {
    true
}

impl Validate for RunCommand {
    fn validate(&mut self, _chroot_path: &PathBuf) -> Result<(), Error> {
        Ok(())
    }
}

impl Command for RunCommand {
    fn build(&mut self, chrootdir: &PathBuf, user: &Option<String>) -> Result<(), Error> {
        let mut c = if self.chroot {
            CommandBuilder::new(chrootdir)?
        } else {
            CommandBuilder::new(&PathBuf::new())?
        };

        if let Some(w) = &self.workdir {
            c.cd(&w, true)?;
        }
        // Since validate is done we can safelly unwrap
        if self.commands.is_none() {
            let cmd = validate_string(&self.command, "RunCommand.command")?;
            c.push(&cmd);
        } else {
            if let Some(cmd) = &self.command {
                c.push(&cmd);
            }

            if let Some(l) = &self.commands {
                for cmd in l {
                    c.push(cmd);
                }
            }
        }
        self.output = c.finish();
        if self.chroot {
            self.rchroot = Some(ChRoot::command(chrootdir, user, &self.output));
        }
        Ok(())
    }

    fn command(&self) -> String {
        self.output.clone()
    }

    fn run(&self) -> Result<(), Error> {
        if let Some(r) = &self.rchroot {
            return r.run();
        }
        Sh::command(&self.user, &self.output).run()
    }
}

impl fmt::Display for RunCommand {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.command())
    }
}

mod tests {
    #[test]
    fn test_run_command() {
        use crate::actions::run_command::*;
        let mut r = RunCommand {
            chroot: false,
            workdir: Some("/tmp".into()),
            command: Some("echo apa".into()),
            ..RunCommand::default()
        };
        assert_eq!(true, r.build(&"/notused".into(), &None).is_ok());
        assert_eq!("cd /tmp && echo apa", r.command());

        let mut r = RunCommand {
            chroot: false,
            workdir: Some("dir".into()),
            command: Some("echo apa".into()),
            ..RunCommand::default()
        };

        let res = r.build(&"/".into(), &None);
        assert_eq!(true, res.is_err());
        assert_eq!(Err(Error::PathNotExist("", "dir".into())), res);
        let mut r = RunCommand {
            chroot: false,
            workdir: Some("/tmp".into()),
            command: Some("echo apa".into()),
            ..RunCommand::default()
        };
        let mut v = Vec::new();
        v.push("ls".into());
        r.commands = Some(v);

        let res = r.build(&"/".into(), &None);
        assert_eq!(true, res.is_ok());
        //let r = res.unwrap();
        assert_eq!("cd /tmp && echo apa && ls", r.command());
    }
}
