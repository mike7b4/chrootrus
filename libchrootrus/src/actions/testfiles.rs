use crate::command::Command;
use crate::errors::Error;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fs::metadata;
use std::path::PathBuf;

#[derive(Deserialize, Serialize)]
pub struct TestFiles {
    workdir: PathBuf,
    files: Vec<PathBuf>,
}

impl Command for TestFiles {
    fn build(&mut self, _: &PathBuf, _: &Option<String>) -> Result<(), Error> {
        Ok(())
    }

    fn command(&self) -> String {
        format!("test on files:{}", self)
    }

    fn run(&self) -> Result<(), Error> {
        for file in &self.files {
            let mut p = self.workdir.clone();
            p.push(file);
            metadata(&p)
                .map(|f| f.is_file())
                .map_err(|e| Error::IO(e, p.clone()))?;
        }
        Ok(())
    }
}

impl fmt::Display for TestFiles {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut s = String::new();
        for file in &self.files {
            s += &format!("\n{}", file.to_string_lossy())
        }
        write!(f, "{}", s)
    }
}
