use crate::chroot::ChRoot;
use crate::command::{Command, CommandBuilder};
use crate::errors::Error;
use crate::validate::*;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::path::PathBuf;

#[derive(Deserialize, Serialize, Default)]
pub struct RepoSync {
    branch: Option<String>,
    manifest: Option<String>,
    workdir: Option<PathBuf>,
    #[serde(skip)]
    output: String,
    #[serde(skip)]
    chroot: Option<ChRoot>,
}

impl Validate for RepoSync {
    fn validate(&mut self, _chroot: &PathBuf) -> Result<(), Error> {
        if let Some(manifest) = &self.manifest {
            if self.branch.is_none() {
                self.branch = Some(String::from("master"));
            }
        }
        Ok(())
    }
}

impl Command for RepoSync {
    fn build(&mut self, chrootdir: &PathBuf, user: &Option<String>) -> Result<(), Error> {
        let mut c = CommandBuilder::new(chrootdir)?;

        if let Some(p) = &self.workdir {
            // true means check if dir exists
            c.cd(p, true)?;
        }

        c.repo_sync(&self.branch, &self.manifest);
        self.output = c.finish();
        self.chroot = Some(ChRoot::command(chrootdir, user, &self.output));
        Ok(())
    }

    fn command(&self) -> String {
        self.output.clone()
    }

    fn run(&self) -> Result<(), Error> {
        if let Some(chroot) = &self.chroot {
            return chroot.run();
        }
        panic!("No chroot did you forget to call build?")
    }
}

impl fmt::Display for RepoSync {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.command())
    }
}

mod tests {
    #[test]
    fn test_reposync() {
        use crate::reposync::*;
        let mut b = RepoSync {
            branch: Some("apa".into()),
            ..RepoSync::default()
        };

        b.build(&"/".into(), &None)
            .expect("Build command should work");
        assert_eq!("repo checkout apa && repo sync", b.command());
    }
}
