use crate::errors::Error;
use std::path::PathBuf;

// TODO change arguments to Recipe, Args for more flexibility
pub trait Validate {
    fn validate(&mut self, parent: &PathBuf) -> Result<(), Error>;
}

/// Validate string.
pub fn validate_string(value: &Option<String>, field: &'static str) -> Result<String, Error> {
    if value.is_none() {
        return Err(Error::MissingField(field));
    }
    Ok(value.clone().unwrap())
}
/// Validate path. Note parent path
/// is prepended to the path before test.
pub fn validate_path(
    parent: &PathBuf,
    path: &Option<PathBuf>,
    field: &'static str,
) -> Result<(), Error> {
    match path {
        Some(p) => {
            let mut np = parent.clone();
            np.push(&p);

            std::fs::metadata(&np).map_err(|_| Error::PathNotExist(field, np.clone()))?;

            Ok(())
        }
        None => Err(Error::MissingField(field)),
    }
}

mod tests {
    #[test]
    fn test_validate_path() {
        use crate::validate::*;
        let path = Some("/tmp".into());
        assert_eq!(true, validate_path(&"".into(), &path, "path").is_ok());
        assert_eq!(Some(PathBuf::from("/tmp")), path);
        assert_eq!(
            true,
            validate_path(&"".into(), &Some("/tmpiiidhj".into()), "path").is_err()
        );
        assert_eq!(
            true,
            validate_path(&"/tmp".into(), &Some("tmp".into()), "path").is_err()
        );

        let path = Some("bin".into());
        assert_eq!(true, validate_path(&"/usr".into(), &path, "path").is_ok());
    }

    #[test]
    fn test_validate_string() {
        use crate::validate::*;
        assert_eq!(true, validate_string(&None, "").is_err());
        assert_eq!(
            String::from("foo"),
            validate_string(&Some("foo".into()), "").unwrap()
        );
    }
}
