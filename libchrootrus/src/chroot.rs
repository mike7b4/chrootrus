use crate::errors::Error;
use std::fmt;
use std::path::PathBuf;
use std::process::{Command, Stdio};
#[derive(Debug)]
pub struct ChRoot {
    args: Vec<String>,
}

impl ChRoot {
    pub fn command(chrootdir: &PathBuf, user: &Option<String>, command: &str) -> Self {
        let mut args: Vec<String> = Vec::new();
        args.push("chroot".into());
        args.push(chrootdir.to_string_lossy().into());
        if let Some(user) = user {
            args.push("su".into());
            args.push("-l".into());
            args.push(user.into());
        }
        args.push("/bin/sh".into());
        args.push("-c".into());
        args.push(command.into());
        Self { args }
    }

    pub fn run(&self) -> Result<(), Error> {
        println!("{:?}", &self.args);
        let mut child = Command::new("sudo")
            .args(&self.args)
            .stdin(Stdio::piped())
            .env("DEBIAN_FRONTEND", "noninteractive")
            .env("DEBCONF_NONINTERACTIVE_SEEN", "true")
            .env("LC_ALL", "C")
            .spawn()
            .unwrap_or_else(|_| panic!("'{:?}' failed", self.args));

        let exitstatus = child.wait();
        if let Ok(exitstatus) = exitstatus {
            if !exitstatus.success() {
                return Err(Error::CommandFailed(
                    self.args.join(" "),
                    exitstatus.code().unwrap_or(127),
                ));
            }
        } else {
            panic!("WTF should not reach this");
        }
        Ok(())
    }
}

impl fmt::Display for ChRoot {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.args.join(" "))
    }
}

mod tests {
    #[test]
    fn test_commands() {
        use crate::chroot::ChRoot;
        let cmd: ChRoot = ChRoot::command(&"/".into(), &None, "echo hello");
        assert_eq!(
            String::from(r#"chroot / /bin/sh -c echo hello"#),
            cmd.to_string()
        );

        let cmd = ChRoot::command(&"/mychroot".into(), &Some("login".into()), "echo hello");
        assert_eq!(
            String::from(r#"chroot /mychroot su -l login /bin/sh -c echo hello"#),
            cmd.to_string()
        );
    }
}
