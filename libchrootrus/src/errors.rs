use std::fmt;
use std::path::PathBuf;
#[derive(Debug)]
pub enum Error {
    CommandExecuteFailed,
    CommandFailed(String, i32),
    IO(std::io::Error, PathBuf),
    NotImplemented(String),
    MissingField(&'static str),
    MissingDefine(&'static str),
    AbsolutePathNotAllowed(PathBuf),
    PathNotExist(&'static str, PathBuf),
    Mount(&'static str),
    Yaml(serde_yaml::Error),
    NoValidRunActionsSpecified,
    FileExists(PathBuf),
    //TomlSE(toml::ser::Error),
}

/// This is sooo ugly needed for unitests
/// There is probably better ways do this
/// Note not all checks are implemented...
/// Some strings are ignored.
impl PartialEq for Error {
    fn eq(&self, other: &Self) -> bool {
        use Error::*;
        match self {
            PathNotExist(_, p) => match other {
                PathNotExist(_, p2) => p == p2,
                _ => false,
            },
            AbsolutePathNotAllowed(p) => match other {
                AbsolutePathNotAllowed(p2) => *p == *p2,
                _ => false,
            },
            NotImplemented(_) => match other {
                NotImplemented(_) => true,
                _ => false,
            },
            MissingField(f) => match other {
                MissingField(f2) => *f == *f2,
                _ => false,
            },
            MissingDefine(f) => match other {
                MissingDefine(f2) => *f == *f2,
                _ => false,
            },
            CommandFailed(_, _) => match other {
                CommandFailed(_, _) => true,
                _ => false,
            },
            IO(io, _) => match other {
                IO(io2, _) => io.kind() == io2.kind(),
                _ => false,
            },
            CommandExecuteFailed => match other {
                CommandExecuteFailed => true,
                _ => false,
            },
            Mount(_) => match other {
                Mount(_) => true,
                _ => false,
            },
            Yaml(_) => match other {
                Yaml(_) => true,
                _ => false,
            },
            FileExists(_) => match other {
                FileExists(_) => true,
                _ => false,
            },
            NoValidRunActionsSpecified => match other {
                NoValidRunActionsSpecified => true,
                _ => false,
            },
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(io: std::io::Error) -> Self {
        Error::IO(io, "?".into())
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(e: serde_yaml::Error) -> Self {
        Error::Yaml(e)
    }
}

impl From<Error> for i32 {
    fn from(e: Error) -> i32 {
        use Error::*;
        match e {
            CommandExecuteFailed => 64,
            IO(_, _) => 65,
            NotImplemented(_) => 66,
            MissingField(_) => 67,
            MissingDefine(_) => 68,
            AbsolutePathNotAllowed(_) => 69,
            PathNotExist(_, _) => 70,
            Mount(_) => 71,
            Yaml(_) => 72,
            NoValidRunActionsSpecified => 73,
            FileExists(_) => 74,
            CommandFailed(_, _) => 127,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Error::*;
        match &*self {
            CommandExecuteFailed => write!(f, "Command execute failed"),
            CommandFailed(cmd, exitcode) => {
                write!(f, "Command: '{}' failed with exitcode {}", cmd, exitcode)
            }
            IO(e, p) => write!(f, "{} on '{}'", e, p.as_path().to_str().unwrap_or("")),
            NotImplemented(action) => write!(f, "Action: '{}' is not implemented!", &action),
            MissingField(field) => write!(f, "Missing field: {}", field),
            MissingDefine(key) => write!(f, "Missing define: {0} please add -D{0}=<value>", key),
            PathNotExist(what, path) => write!(
                f,
                "{}: '{}' does not exist or is not accessable!",
                what,
                path.to_string_lossy()
            ),
            AbsolutePathNotAllowed(path) => {
                write!(f, "Absolute paths is not allowed! '{:?}'", path)
            }
            Mount(s) => write!(f, "Mount of '{}' failed", s),
            Yaml(e) => write!(f, "Recipe error in Yaml: {}", e),
            FileExists(ff) => write!(f, "File: '{}' already exists", ff.to_string_lossy()),
            NoValidRunActionsSpecified => write!(f, "No valid run actions specified"),
        }
    }
}

mod tests {
    #[test]
    fn test_eq() {
        use crate::errors::Error::*;
        assert_eq!(CommandExecuteFailed, CommandExecuteFailed);
        assert_eq!(
            true,
            PathNotExist("", "apa".into()) == PathNotExist("", "apa".into())
        );
        assert_eq!(
            false,
            PathNotExist("", "notequal".into()) == PathNotExist("", "apa".into())
        );

        // mount does not care what's in string
        assert_eq!(true, Mount("apa2") == Mount("apa"));
        assert_eq!(true, Mount("apa") == Mount("apa"));

        // MissingField does care what's in string
        assert_eq!(false, MissingField("apa2") == MissingField("apa"));
        assert_eq!(true, MissingField("apa") == MissingField("apa"));

        // Missing define does care what's in string
        assert_eq!(false, MissingDefine("apa2") == MissingDefine("apa"));
        assert_eq!(true, MissingDefine("apa") == MissingDefine("apa"));

        // ofcourse this is not equal
        assert_eq!(false, MissingDefine("apa") == MissingField("apa"));
    }
}
