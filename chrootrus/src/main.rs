use dirs;
use libchrootrus::chroot::ChRoot;
use libchrootrus::errors::Error;
use libchrootrus::recipe::Recipe;
use libchrootrus::validate::*;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Args {
    #[structopt(long)]
    recipe: Option<PathBuf>,
    #[structopt(long, help = "path to new root")]
    chroot: Option<PathBuf>,
    #[structopt(long, help = "path to workdir")]
    workdir: Option<PathBuf>,
    #[structopt(long, short = "l", help = "su <USER> inside chroot")]
    user: Option<String>,
    #[structopt(long, short, help = "command to run")]
    command: Option<String>,
    #[structopt(long, short, help = "Do not run chroot just print commands")]
    simulate: bool,
    #[structopt(help = "if no actions specified all actions will be run")]
    run_actions: Vec<String>,
    #[structopt(long)]
    list: bool,
}

fn run() -> Result<(), Error> {
    let mut args = Args::from_args();
    if args.recipe.is_none() {
        args.recipe = Some(PathBuf::from(&format!(
            "{}/chrootrus/recipe.yaml",
            dirs::config_dir().unwrap().to_string_lossy()
        )));
    }

    if let Some(recipe_file) = args.recipe {
        let mut file = File::open(&recipe_file).map_err(|e| Error::IO(e, recipe_file.clone()))?;
        let mut s = String::new();
        file.read_to_string(&mut s)
            .map_err(|e| Error::IO(e, recipe_file.clone()))?;
        let mut r: Recipe = serde_yaml::from_str(&s)?; //.map_err(|e|Error::IO(e, recipe))?;
        if args.list {
            // TODO this is ugly
            r.list();
            std::process::exit(0);
        }
        r.validate(&args.workdir.unwrap_or_else(|| ".".into()))?;
        // Note path, user not used since we expect it in yaml
        r.build(&PathBuf::new(), &None)?;
        r.run(&args.run_actions, args.simulate)?;
    } else if let Some(chroot) = &args.chroot {
        let mut cmd = args
            .command
            .ok_or(Error::MissingField("--command or --recipe"))?;
        args.workdir
            .map(|w| {
                cmd += &w.to_string_lossy();
            })
            .ok_or(Error::MissingField("--command"))?;
        let chroot = ChRoot::command(&chroot, &args.user, &cmd);
        chroot.run()?;
    } else {
        return Err(Error::MissingField("--path and --command or --recipe"));
    }

    Ok(())
}

fn main() {
    run().unwrap_or_else(|e| {
        use ansi_term::Colour::Red;
        eprintln!("{}", Red.paint(e.to_string()));
        std::process::exit(i32::from(e));
    });
}
