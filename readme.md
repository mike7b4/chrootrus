# chrootrus

Use yaml recipes to build stuff inside a chroot enviroment.

# Actions available:

 - RepoSync
 - Make
 - RunCommand (inside or outside chroot)
 - TestFiles (run outside chroot)
 - CreateFile (run outside chroot)
 - Mount


